# Flan's Official Content Packs

A modpack containing Flan's Mod and all it's official Content Packs.

[More info](https://gitlab.com/TechnicalMa/technic-flans)

<small>Neither this pack nor TechnicalMa is associated with Flan's mod.</small>