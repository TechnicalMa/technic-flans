#!/usr/bin/env python3

# Copyright 2020 TechnicalMa <ma_124+mc@pm.me>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import tempfile
import zipfile

import requests
import yaml
import click
import os
import hashlib
import urllib.parse as urlp


def trim_prefixes(s, *ps):
    for p in ps:
        if s.startswith(p):
            return s[len(p) :]
    return s


def trim_suffixes(s, *ps):
    for p in ps:
        if s.endswith(p):
            return s[: len(s) - len(p)]
    return s


_cache = False
cache_path = ".cache"


def cache_name(n):
    return os.path.join(cache_path, hashlib.sha224(n.encode()).hexdigest())


def download(url, to, label, cache):
    print(
        "!! {} ({} <- {})".format(label, to, url)
    )  # TODO remove and fix progress bars
    if not _cache:
        cache = False

    if cache:
        try:
            with open(cache_name(url), "rb") as f:
                copy(f, to, label + " (cached)")
            return
        except FileNotFoundError:
            pass

    if url.startswith("https://adfoc.us/serve") or url.startswith(
        "http://adfoc.us/serve"
    ):
        url = urlp.parse_qs(urlp.urlparse(url).query)["url"][0]
    elif url.startswith("https://drive.google.com/open") or url.startswith(
        "http://drive.google.com/open"
    ):
        url = (
            "https://docs.google.com/uc?export=download&id="
            + urlp.parse_qs(urlp.urlparse(url).query)["id"][0]
        )

    url = trim_prefixes(url, "file://")

    if url.startswith("http://") or url.startswith("https://"):
        response = requests.get(url, stream=True)
        total_length = response.headers.get("content-length")

        if cache:
            with open(cache_name(url), "wb") as cachef:
                _download(response, total_length, to, cachef, label)
        else:
            _download(response, total_length, to, None, label)
    else:
        with open(url, "rb") as orig:
            copy(orig, to, label)


def _download(response, total_length, to, cache, label):
    if total_length is None:  # no content length header
        click.echo(label)
        to.write(response.content)
    else:
        with click.progressbar(length=int(total_length), label=label) as bar:
            dl = 0
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                to.write(data)
                if cache is not None:
                    cache.write(data)
                bar.update(dl)
            bar.finish()


def copy(orig, new, label):
    if label == "":
        while True:
            data = orig.read(4096)
            if not data:
                break
            new.write(data)
        return

    dl = 0
    with click.progressbar(length=os.fstat(orig.fileno()).st_size, label=label) as bar:
        while True:
            data = orig.read(4096)
            if not data:
                break
            new.write(data)
            dl += len(data)
            bar.update(dl)
    bar.finish()


def mkdir(ar: zipfile.ZipFile, param):
    pass  # zipfile does that automatically


@click.command()
@click.option("--name", type=str)
@click.option("--cache", default=True, type=bool)
@click.argument("config", type=click.File("r", encoding="UTF-8"))
def create(name, cache, config):
    global _cache
    _cache = cache
    _create(name, config)


def newzip(name):
    return zipfile.ZipFile(name, "w", compression=zipfile.ZIP_DEFLATED, compresslevel=3)


def _create(name, config):
    global cache_path
    if _cache:
        cache_path = os.path.abspath(".cache")
        try:
            os.mkdir(".cache", 0o755)
        except FileExistsError:
            pass

    os.chdir(os.path.dirname(os.path.normpath(config.name)))

    cfg = None
    with config:
        if name == "" or name is None:
            name = trim_suffixes(
                str(os.path.basename(os.path.normpath(config.name))), ".yaml", ".yml"
            )
        cfg = yaml.safe_load(config)

    if name == "" or name is None:
        click.secho("Could not find the name of this pack.", fg="red")
        click.echo("You can explicitly set this using the --name flag.")
        exit(1)

    with newzip(name + ".zip") as ar:
        mkdir(ar, "bin")
        mkdir(ar, "mods")
        mkdir(ar, "Flans")

        with ar.open("bin/modpack.jar", "w") as f:
            download(cfg["forge"], f, "Downloading Forge", True)

        with ar.open("mods/flans.jar", "w") as f:
            download(cfg["flans"], f, "Downloading Flans", True)

        for k in cfg["include"].keys():
            with ar.open(k, "w") as f:
                download(cfg["include"][k], f, "Downloading " + k, False)

        for k in cfg["content-packs"].keys():
            if "changes" not in cfg or k not in cfg["changes"]:
                with ar.open("Flan/" + k + ".jar", "w") as f:
                    download(cfg["content-packs"][k], f, "Downloading " + k, True)
            else:
                with ar.open("Flan/" + k + ".jar", "w") as new:
                    with tempfile.TemporaryFile() as orig:
                        download(
                            cfg["content-packs"][k], orig, "Downloading " + k, True
                        )
                        orig.seek(0, 0)
                        with zipfile.ZipFile(orig, "r") as origz:
                            with newzip(new) as newz:
                                infos = origz.infolist()
                                with click.progressbar(
                                    infos, len(infos), "Changing " + k
                                ) as bar:
                                    for info in bar:
                                        if info.filename not in cfg["changes"][k]:
                                            with newz.open(info.filename, "w") as newf:
                                                with origz.open(
                                                    info.filename, "r"
                                                ) as origf:
                                                    copy(origf, newf, "")
                                        else:
                                            with newz.open(info.filename, "w") as newf:
                                                with origz.open(
                                                    info.filename, "r"
                                                ) as origf:
                                                    while True:
                                                        line = origf.readline()
                                                        if not line:
                                                            break
                                                        ps = line.split()
                                                        if (
                                                            len(ps) > 0
                                                            and ps[0].decode()
                                                            in cfg["changes"][k][
                                                                info.filename
                                                            ]
                                                        ):
                                                            newf.write(
                                                                (
                                                                    line.split()[
                                                                        0
                                                                    ].decode()
                                                                    + " "
                                                                    + str(
                                                                        cfg["changes"][
                                                                            k
                                                                        ][
                                                                            info.filename
                                                                        ][
                                                                            line.split()[
                                                                                0
                                                                            ].decode()
                                                                        ]
                                                                    )
                                                                    + "\r\n"
                                                                ).encode()
                                                            )
                                                        else:
                                                            newf.write(line)


@click.group(help="This is for internal usage only.")
def internal():
    pass


@internal.command()
def buildall():
    global _cache
    _cache = True
    for root, dirs, files in os.walk("."):
        for f in files:
            f = os.path.join(root, f)
            if (
                str(f).endswith(".yaml")
                or str(f).endswith(".yml")
                and str(f) != ".gitlab-ci.yml"
            ):
                print("BUILDING {}".format(f))
                wd = os.getcwd()
                _create(None, open(f, "r"))
                os.chdir(wd)


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "internal":
        sys.argv[1] = sys.argv[0] + " internal"
        sys.argv = sys.argv[1:]
        internal()
    else:
        create()
