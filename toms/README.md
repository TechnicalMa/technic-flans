# Tom's Flans Content Packs

A modpack based on Flan's Mod and all it's official Content Packs.
Some values have been tweaked.

[More info](https://gitlab.com/TechnicalMa/technic-flans)

[Play it on Technic Launcher](https://www.technicpack.net/modpack/flans-mod-mc-1122.1667233)

<small>Neither this pack nor TechnicalMa is associated with Flan's mod.</small>